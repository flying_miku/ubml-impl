/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociationKey;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of GspAssoKey
 *
 * @ClassName: GspAssoKeyDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssoKeyDeserializer extends JsonDeserializer<GspAssociationKey> {

    public GspAssoKeyDeserializer() {
    }

    @Override
    public GspAssociationKey deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeGspAssociation(jsonParser);
    }

    public GspAssociationKey deserializeGspAssociation(JsonParser jsonParser) {
        GspAssociationKey associationKey = new GspAssociationKey();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(associationKey, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return associationKey;
    }

    private void readPropertyValue(GspAssociationKey associationKey, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.SOURCE_ELEMENT:
                associationKey.setSourceElement(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.TARGET_ELEMENT:
                associationKey.setTargetElement(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.SOURCE_ELEMENT_DISPLAY:
                associationKey.setSourceElementDisplay(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.TARGET_ELEMENT_DISPLAY:
                associationKey.setTargetElementDisplay(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.REF_DATA_MODEL_NAME:
                associationKey.setRefDataModelName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
        }
    }
}
