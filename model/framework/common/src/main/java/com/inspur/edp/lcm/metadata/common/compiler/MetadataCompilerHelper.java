/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.compiler;

import com.inspur.edp.lcm.metadata.api.entity.compiler.MetadataCompilerConfiguration;
import com.inspur.edp.lcm.metadata.spi.MetadataCompileAction;
import java.util.List;

public class MetadataCompilerHelper extends MetadataCompilerConfigLoader {
    private static MetadataCompilerHelper singleton = null;

    public static MetadataCompilerHelper getInstance() {
        if (singleton == null) {
            singleton = new MetadataCompilerHelper();
        }
        return singleton;
    }

    public List<MetadataCompilerConfiguration> getCompileConfigrationList() {
        return loadCompileConfigration();
    }

    public MetadataCompileAction getManager(String typeCode) {
        MetadataCompileAction manager = null;
        MetadataCompilerConfiguration compileConfigurationData = getCompileConfigurationData(typeCode);
        if (compileConfigurationData != null) {
            Class<?> cls;
            try {
                if (compileConfigurationData.getMetadataCompiler() != null) {
                    cls = Class.forName(compileConfigurationData.getMetadataCompiler().getName());
                    manager = (MetadataCompileAction) cls.newInstance();
                }
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return manager;
    }
}
