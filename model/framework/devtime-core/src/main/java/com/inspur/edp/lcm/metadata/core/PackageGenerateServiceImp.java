/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core;

import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import com.inspur.edp.lcm.metadata.api.service.PackageGenerateService;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import java.util.List;

/**
 * @author zhaoleitr
 */
public class PackageGenerateServiceImp implements PackageGenerateService {
    private PackageGenerateCoreService packageGenerateCoreService = new PackageGenerateCoreService();

    @Override
    public void generatePackage(String path) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        packageGenerateCoreService.generatePackage(absolutePath);
    }

    @Override
    public List<MetadataPackage> getLocalPackages(String path) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        return packageGenerateCoreService.getLocalPackages(absolutePath);
    }
}
