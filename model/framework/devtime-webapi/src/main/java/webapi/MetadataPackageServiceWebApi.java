/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package webapi;

import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageDto;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.api.service.PackageGenerateService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.io.File;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

public class MetadataPackageServiceWebApi {
    PackageGenerateService metadataPackageService;

    private PackageGenerateService getMetadataPackageService() {
        if (metadataPackageService == null) {
            metadataPackageService = SpringBeanUtils.getBean(PackageGenerateService.class);
        }
        return metadataPackageService;
    }

    private MetadataService metadataService;

    private MetadataService getMetadataService() {
        if (metadataService == null) {
            metadataService = SpringBeanUtils.getBean(MetadataService.class);
        }
        return metadataService;
    }

    /**
     * 生成元数据包
     *
     * @param metadataPackageDto 实体中的ProjectPath必填，其他值不需要
     */
    @POST
    public void generatePackage(MetadataPackageDto metadataPackageDto) {
        getMetadataPackageService().generatePackage(metadataPackageDto.getProjectPath());
    }

    /**
     * 获取元数据包的基本信息
     *
     * @param packagePath 元数据包路径+元数据包名称，要带后缀.mdpkg，需要Encode，这里暂时定的是全路径，因为元数据包也是固定存放在开发服务器一个目录下，但是不是开发根路径
     * @return
     */
    @GET
    @Path("/packagePath")
    public MetadataPackage getMetadataPackageInfo(@QueryParam(value = "packagePath") String packagePath) {
        File file = new File(packagePath);
        return getMetadataService().getMetadataPackageInfo(file.getName(), file.getParent());
    }

    @GET
    @Path("local")
    public List<MetadataPackage> getLocalPackages(@QueryParam(value = "path") String path) {
        return getMetadataPackageService().getLocalPackages(path);
    }
}
