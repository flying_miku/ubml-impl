/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.model;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.be.definition.GspBusinessEntity;
import org.openatom.ubml.model.be.definition.beenum.BECategory;
import org.openatom.ubml.model.be.definition.beenum.GspDataLockType;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.common.InternalActionUtil;
import org.openatom.ubml.model.be.definition.json.object.BizObjectDeserializer;
import org.openatom.ubml.model.be.definition.json.operation.BizMgrActionCollectionDeserializer;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.collection.BizMgrActionCollection;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.entity.GspCommonModel;
import org.openatom.ubml.model.common.definition.commonmodel.json.model.CommonModelDeserializer;
import org.openatom.ubml.model.common.definition.commonmodel.json.object.CmObjectDeserializer;

/**
 * The  Josn Deserializer Of Business Entity
 *
 * @ClassName: BizEntityDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizEntityDeserializer extends CommonModelDeserializer {

    @Override
    protected boolean readExtendModelProperty(GspCommonModel cm, String propName, JsonParser reader) {
        GspBusinessEntity be = (GspBusinessEntity)cm;
        boolean hasProperty = true;
        switch (propName) {
            case BizEntityJsonConst.CATEGORY:
                be.setCategory(SerializerUtils.readPropertyValue_Enum(reader, BECategory.class, BECategory.values()));
                break;
            case BizEntityJsonConst.DATA_LOCK_TYPE:
                be.setDataLockType(SerializerUtils.readPropertyValue_Enum(reader, GspDataLockType.class, GspDataLockType.values()));
                break;
            case BizEntityJsonConst.EXTEND_TYPE:
                SerializerUtils.readPropertyValue_String(reader);
                break;
            case BizEntityJsonConst.DEPENDENT_ENTITY_ID:
                be.setDependentEntityId(SerializerUtils.readPropertyValue_String(reader));
                break;
            case BizEntityJsonConst.DEPENDENT_ENTITY_NAME:
                be.setDependentEntityName(SerializerUtils.readPropertyValue_String(reader));
                break;
            case BizEntityJsonConst.CACHE_CONFIGURATION:
                be.setCacheConfiguration(SerializerUtils.readPropertyValue_String(reader));
                break;
            case BizEntityJsonConst.ENTITY_PACKAGE_NAME:
                be.setDependentEntityPackageName(SerializerUtils.readPropertyValue_String(reader));
                break;
            case BizEntityJsonConst.ENABLE_CACHING:
                be.setEnableCaching(SerializerUtils.readPropertyValue_boolean(reader));
                break;
            case BizEntityJsonConst.ENABLE_TREE_DTM:
                be.setEnableTreeDtm(SerializerUtils.readPropertyValue_boolean(reader));
                break;
            case BizEntityJsonConst.IS_USING_TIME_STAMP:
                be.setIsUsingTimeStamp(SerializerUtils.readPropertyValue_boolean(reader));
                break;
            case BizEntityJsonConst.BIZ_MGR_ACTIONS:
                readBizMgrActions(reader, be);
                break;
            case BizEntityJsonConst.COMPONENT_ASSEMBLY_NAME:
                be.setComponentAssemblyName(SerializerUtils.readPropertyValue_String(reader));
                break;
            case BizEntityJsonConst.ASSEMBLY_NAME:
                SerializerUtils.readPropertyValue_String(reader);
                break;
            case BizEntityJsonConst.AUTHORIZATIONS:
                readAuthorizations(reader, be);
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }

    private void readAuthorizations(JsonParser reader, GspBusinessEntity be) {
        //未序列化
        SerializerUtils.readStartArray(reader);
        SerializerUtils.readEndArray(reader);
    }

    private void readBizMgrActions(JsonParser jsonParser, GspBusinessEntity be) {
        BizMgrActionCollectionDeserializer deserializer = new BizMgrActionCollectionDeserializer();
        BizMgrActionCollection actionCollection = deserializer.deserialize(jsonParser, null);
        for (BizOperation action : actionCollection) {
            if (!InternalActionUtil.InternalMgrActionIDs.contains(action.getID())) {
                be.getBizMgrActions().add(action);
            }
        }
    }

    @Override
    protected GspCommonModel createCommonModel() {
        return new GspBusinessEntity();
    }

    @Override
    protected CmObjectDeserializer createCmObjectDeserializer() {
        return new BizObjectDeserializer();
    }
}
