/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.beenum;

/**
 * The Type Of Business Entity Determination
 *
 * @ClassName: BEDeterminationType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum BEDeterminationType {
    /**
     * 非持久化: 所对应的计算结果字段是虚拟列，不需要持久化。 此时只需要读取和修改后进行计算
     */
    Transient(0),

    /**
     * 持久化: 所对应的计算结果字段是持久化的。数据保存前需要计算
     */
    Persistent(1);

    private static java.util.HashMap<Integer, BEDeterminationType> mappings;
    private int intValue;

    private BEDeterminationType(int value) {
        intValue = value;
        BEDeterminationType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, BEDeterminationType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, BEDeterminationType>();
        }
        return mappings;
    }

    public static BEDeterminationType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}